To install.
Create tar of entire directory.
  From parent directory: 
    $ tar -cvf smile-installer.tar [dir_name]/*

Copy directory to smile plug and untar:
  From smile plug via ssh: 
    $ tar -xvf smile-installer.tar
    $ cd [dir_name]
    $ ./install_smile_web

To create a compatible KA-Lite content USB stick. Download content for KA-lite. Copy contents of 'content' directory to the root of the usb. Create a file name '.ka-lite.content'. When plugged in, this USB drive will automatically mount and create a symlink in /usr/share/ka-lite/kalite/content'.
